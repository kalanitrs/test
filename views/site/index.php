<?php

/* @var $this yii\web\View */

$this->title = 'Task Manager';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome!</h1>

        <p class="lead">Welcome to a task manager application!<br>This application would help you to manage your and your workers' tasks.<br>Enjoy!</p>

    </div>
</div>
