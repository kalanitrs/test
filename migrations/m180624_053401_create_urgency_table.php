<?php

use yii\db\Migration;

/**
 * Handles the creation of table `urgency`.
 */
class m180624_053401_create_urgency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('urgency', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);

        $this->insert('urgency', [
            'id' => '1',
            'name' => 'critical',
        ]);

        $this->insert('urgency', [
            'id' => '2',
            'name' => 'normal',
        ]);

        $this->insert('urgency', [
            'id' => '3',
            'name' => 'low',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('urgency');
    }
}
