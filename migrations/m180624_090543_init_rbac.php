<?php

use yii\db\Migration;

/**
 * Class m180624_090543_init_rbac
 */
class m180624_090543_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $manager = $auth->createRole('manager');
        $auth->add($manager);

        $employee = $auth->createRole('employee');
        $auth->add($employee);
              
        $auth->addChild($manager, $employee);

        $createTask = $auth->createPermission('createTask');
        $auth->add($createTask);

        $viewTasks = $auth->createPermission('viewTasks');
        $auth->add($viewTasks);

        $editOwnProfile = $auth->createPermission('editOwnProfile');

        $rule = new \app\rbac\SelfRule;
        $auth->add($rule);
        $editOwnProfile->ruleName = $rule->name;
        $auth->add($editOwnProfile);

        $viewProfiles = $auth->createPermission('viewProfiles');
        $auth->add($viewProfiles);

        $editProfile = $auth->createPermission('editProfile');
        $auth->add($editProfile);

        $manageTask = $auth->createPermission('manageTask');
        $auth->add($manageTask);

        $auth->addChild($employee, $createTask);
        $auth->addChild($employee, $viewTasks);
        $auth->addChild($employee, $editOwnProfile);
        $auth->addChild($employee, $viewProfiles);

        $auth->addChild($manager, $editProfile);
        $auth->addChild($manager, $manageTask);

        $auth->addChild($editOwnProfile, $editProfile);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_090543_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_090543_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
